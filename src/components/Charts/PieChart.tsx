import React, { useState, useEffect } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { IProducts } from '../../interfaces/dashboard.interface';

ChartJS.register(ArcElement, Tooltip, Legend);
 
export const PieChart = ({products}: IProducts|any) => {
  const generateColor = (tipoCor: number = 0) => {
    const randomNumber = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1) + min);
    const randomByte = () => randomNumber(0, 255);
    // const randomPercent = () => (randomNumber(50, 100) * 0.01).toFixed(2);
    const randomCssRgba = 'rgba('+randomByte().toString()+','+randomByte().toString()+','+randomByte().toString()+','+0.6+')'; //randomPercent()+')';

    return randomCssRgba;
  }

  const [data, setData] = useState({
    labels: [''],
    datasets: [
      {
        label: '',
        data: [0],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
        ],
        borderWidth: 1,
      },
    ],
  });

  useEffect(() => {
    if (products) {
      let newProdsMap = new Map([...products.map((e: any) => [e.grupo])]);
      let prodsLabels: any[] = Array.from(newProdsMap, ([name, value]) => (name));
     
      let prodsGroups: any[] = [];
      let prodCounter = 0;
      
      for (let c1 = 0; c1 < prodsLabels.length; c1++) {
        for (let c2 = 0; c2 < products.length; c2++) {
          if (products[c2].grupo === prodsLabels[c1]) prodCounter++;
        }
        prodsGroups.push(prodCounter);
        prodCounter = 0;  
      }

      let pieGroupsColors = prodsGroups.map(e => generateColor());
      let pieBordersColors = pieGroupsColors.map(e => e.substring(0, e.lastIndexOf(',')+1)+'1)');
      
      setData({
        labels: prodsLabels,
        datasets: [
          {
            label: 'Qtde. Produtos por Grupo',
            data: prodsGroups,
            backgroundColor: pieGroupsColors,
            borderColor: pieBordersColors,
            borderWidth: 1,
          },
        ],
      });
    }
  },[products])

  return <Pie data={data} />;
}