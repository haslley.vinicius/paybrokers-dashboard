import React, { useEffect, useState } from 'react';
import {
    Box,
    TextField,
    FormControl,
    InputLabel,
    OutlinedInput,
    InputAdornment,
    IconButton,
    Button,
    FormHelperText,
} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import PayBrokersLogo from '../../assets/images/paybrokers_logo.png';
import { useNavigate } from "react-router-dom";
import {
    execLogin
} from '../../services/Login/login.service';
import { userLogin } from '../../features/loginSlice';
import { useAppDispatch } from '../../store/hooks';

const Login = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const [values, setValues] = useState<any>({ 
        userInputError: false,
        passInputError: false,
        userName: "",       
        password: "",       
        showPassword: false,
        userNotFound: false
    });
    
    const handleChange = (prop: any) => (event: React.ChangeEvent<HTMLInputElement>) => {
            setValues({ 
                ...values, 
                [prop]: event.target.value,
                userInputError: (prop === "userInputError" || prop === "userName") ? !event.target.value.length ? true : false : values.userInputError,
                passInputError: (prop === "passInputError" || prop === "password") ? !event.target.value.length ? true : false : values.passInputError                
            });
        };

    const handleInputError = (prop: any) => (event: React.FocusEvent<HTMLInputElement>) => {
        setValues({
            ...values,
            [prop]: !event.target.value.length ? true : false            
        });        
    }

    const handleClickShowPassword = () => {        
            setValues({
                ...values,
                showPassword: !values.showPassword,
            });
        };

    const handleMouseDownPassword = (
        event: React.MouseEvent<HTMLButtonElement>
    ) => {
        event.preventDefault();
    };

    const login = async () => {
        if (!values.userName.length || !values.password.length) return;  

        const resp = await execLogin(values.userName, values.password);

        if (resp.length) {   
            setValues({
                ...values,
                userNotFound: false
            });

            const userData = {
                user: values.userName,
                password: values.password
            }

            localStorage.setItem('userName', userData.user);
            localStorage.setItem('isLogged', 'true');

            dispatch(userLogin(userData));

            goToDashboard();
        } else {
            setValues({
                ...values,
                userNotFound: true
            })
        }
    }

    const goToDashboard = () => {
        navigate("dashboard");
    };

    useEffect(() => {
        if (Boolean(localStorage.getItem('isLogged'))) {
            localStorage.removeItem('userName');
            localStorage.removeItem('products');
            localStorage.removeItem('isLogged');
        }
    },[])

    return (
        <Box className="login-container">
            <Box>
                <Box style={{ marginBottom: "30px" }}>
                    <img src={PayBrokersLogo} alt="paybrokers_logo" />
                    <div>{'PayBrokers Dashboard'}</div>
                </Box>
                {
                    values.userNotFound ? 
                        <Box className="user-not-found">
                            {'Usuário não encontrado'}
                        </Box>
                    :
                        <></>
                }
                <Box
                    component="form"
                    noValidate
                    autoComplete="off"
                    sx={{ witdh: "300px" }}
                >
                    <TextField
                        error={values.userInputError}
                        id="user-field"
                        label={'User'}
                        type="text"
                        className="user-input"
                        value={values.userName}
                        onChange={handleChange("userName")}
                        onBlur={handleInputError("userInputError")}
                        helperText={values.userInputError ? "Campo necessário" : ""}
                    />
                    <br />
                    <FormControl
                        variant="outlined"
                        style={{
                            marginTop: "20px",
                            marginBottom: "20px",
                        }}
                    >
                        <InputLabel htmlFor="outlined-adornment-password">
                            Password
                        </InputLabel>
                        <OutlinedInput
                            error={values.passInputError}
                            id="outlined-adornment-password"
                            type={values.showPassword ? "text" : "password"}
                            value={values.password}
                            onChange={handleChange("password")}
                            onBlur={handleInputError("passInputError")}                    
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="Password"
                            className="password-input"
                        />
                        {!!values.passInputError && (
                            <FormHelperText error id="accountId-error">
                                {'Campo necessário'}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <br />
                    <Button
                        variant="contained"
                        className="std-button user-input"   
                        onClick={login}                 
                    >
                        {'Login'}
                    </Button>
                    <br />
                </Box>      
            </Box>      
        </Box>
    )
}

export default Login;