# PayBrokers Dashboard (Sketch Project)
## PayBrokers Dashboard - Descrição

Projeto de aplicação web-dashboard, utilizando as tecnologias React, Typescript REDUX (reduxjs/toolkit), 
React Router, react-chartjs-2, Material-UI, Styled-Components e JSON Server no server-side.
Esta aplicação funciona no modelo server-client, possibilita  a visualização de gráficos de vendas, 
listagem, detalhamento, remoção e inclusão de produtos.

Projeto desenvolvido para a empresa PayBrokers 
Site: https://www.paybrokers.com.br/

## Esta documentação descreve

- Como configurar o ambiente
- Como rodar a aplicação 
- Como utilizar a aplicação;

> Neste tópico, estão os conceitos fundamentais para a configuração do ambiente.

## Tecnologias necessárias à execução da aplicação

1) Ambiente de desenvolvimento: ex. VSCode, ATOM, IntelliJ IDE etc.
2) NodeJS (versão utilizada 16.16.0)
3) NPM (versão utilizada 8.11.0)
4) Yarn (versão utilizada 1.22.19)

Para executar a instalação da aplicação, é necessário clonar o repositório através do link:
`https://gitlab.com/haslley.vinicius/paybrokers-dashboard.git`

- necessário possuir uma conta Git. 

# Pós clonagem
Após clonar o repositório, deve-se executar as seguintes ações:

1) Entrar na pasta paybrokers-dashboard;
2) Abrir duas janelas de terminal separadas, sendo uma para executar a aplicação e a outra para executar 
o servidor via JSON Server; 
3) Estando na pasta paybrokers-dashboard via terminal, o mesmo deve estar apontando para esta mesma pasta, 
executar o comando 'yarn' ou 'npm install' (aguardar a finalização do processo) 
4) Após finalizada a instalação dos módulos necessários, executar o comando 'yarn start' ou 'npm start', este
comando irá executar a aplicação web na porta 3000. (PS: Deixar o terminal da aplicação web em execução)
5) Na outra janela de terminal, o mesmo deve estar apontando também para a pasta da aplicação, executar o 
comando 'yarn server' ou 'npm server', este comando irá executar a aplicação server (via JSON Server) na porta 3500. (PS: Deixar também o terminal da aplicação server em execução)

## IMPORTANTE
`yarn` ou `npm install`
`yarn start`ou `npm start`

Estes comandos devem ser executados via terminal, ambos com o terminal estando direcionado na pasta da 
aplicação, obedecendo a ordem:
1-> aplicação-web
2-> aplicação server

### Execução da aplicação

Para utilizar a aplicação, com ambos os terminais (aplicação web e aplicação server) em execução, basta abrir o 
seu broswer (caso o mesmo não execute automaticamente após inciar a aplicação web) e digitar `localhost:3000`, 
para utilizar a aplicação server de forma manual, pode-se utilizar um client de requests como o Insomnia, Postman ou outro de sua escolha, e adicionar os seguintes verbos:

GET: `localhost:3500/users` (listagem dos usuários ativos/cadastrados)
-> usuários existentes: user1, user2 e user3, suas respectivas senhas são pass1, pass2, pass3;

GET: `localhost:3500/products` (listagem de produtos previamente cadastrados)

POST: `localhost:3500/users` (adicionar um novo usuário via client request: ex. Insomnia ou Postman)
-> o payload para adicionar usuários deve seguir o padrão: {"id": number, "user": string, "password": string}

POST `localhost:3500/products` (adiciona um novo produto à lista, pode ser feito tanto via client request quanto via 
aplicação web)
-> o payload para adicionar novos produtos deve seguir o padrão: {"id": string, "nome": string, "valor": number(float), 
"grupo": string, "vendido": boolean, "dataVenda": string no formato "DD-MM-YYYY", "descricao": string}

`PS1: Foi adicionado um fator de estimativa/multiplicação para o valor de venda diária, sendo o mínimo para este 3x, `
`mas podendo ser alterado no campo acima do gráfico 'valor total de vendas por dia no mês corrente', o gráfico de `
`barras irá atualizar automaticamente conforme o fator de estimativa/multiplicação para o valor de venda diária.`
`PS2: As cores do gráfico de pizza são gerado randomicamente a cada load.`