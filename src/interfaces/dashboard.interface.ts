export interface IProducts {
    id: string;
    nome: string;
    valor: number;
    grupo: string;
    vendido: true;
    dataVenda: string;
    descricao: string;
}