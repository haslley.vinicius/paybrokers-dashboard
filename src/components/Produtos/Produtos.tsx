import React, { useState } from 'react';
import { 
    Box, 
    Grid, 
    IconButton, 
    List, 
    ListItem, 
    ListItemText, 
    Typography,
    Tooltip,
    Button,
    Modal,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import AssignmentIcon from '@mui/icons-material/Assignment';
import { IProducts } from '../../interfaces/dashboard.interface';
import {
    registerProduct,
    deleteProduct
} from '../../services/Produtos/produtos.service';
import { useAppDispatch } from '../../store/hooks';
import { getProducts } from '../../features/dashboardSlice';

const Produtos = ({products}: IProducts|any) => {
    const [modalDetail, openModalDetail] = useState(false);
    const [modalCad, openModalCad] = useState(false);
    const [modalDel, openModalDel] = useState(false);
    const [produtoInfo, setProdutoInfo] = useState<IProducts|any>({});
    const handleOpenModalDetail = () => openModalDetail(true);
    const handleCloseModalDetail = () => openModalDetail(false);

    const handleOpenModalDel = (prod: IProducts|any, idx: number|any) => {
        setProdutoInfo(prod)
        openModalDel(true);
    }

    const handleCloseModalDel = () => openModalDel(false);
    const handleOpenModalCad = () => openModalCad(true);
    
    const handleCloseModalCad = () => {
        handleCleanCadData();
        openModalCad(false);
    }
    
    const dispatch = useAppDispatch();
    
    const getCadDate = () => {
        let today = new Date();
        let cadDate = today.getDate()+'-'+((today.getMonth()+1).toString().length === 1 ? ('0'+(today.getMonth()+1)) : (today.getMonth()+1))+'-'+today.getFullYear();
        return cadDate;
    };
    
    const [values, setValues] = useState<any>({ 
        idInputError: false,
        nomeInputError: false,
        valorInputError: false,
        grupoInputError: false,
        descricaoInputError: false,
        id: "",
        nome: "",
        valor: 0,
        grupo: "",
        vendido: true,
        dataVenda: getCadDate(),
        descricao: ""
    });

    const handleCleanCadData = () => {
        setValues({ 
            ...values, 
            idInputError: false,
            nomeInputError: false,
            valorInputError: false,
            grupoInputError: false,
            descricaoInputError: false,
            id: "",
            nome: "",
            valor: 0,
            grupo: "",
            vendido: true,
            dataVenda: getCadDate(),
            descricao: ""
        });
    }

    const handleChange = (prop: any) => (event: React.ChangeEvent<HTMLInputElement>) => {
        setValues({ 
            ...values, 
            [prop]: event.target.value,
            idInputError: (prop === "idInputError" || prop === "id") ? !event.target.value.length ? true : false : values.idInputError,
            nomeInputError: (prop === "nomeInputError" || prop === "nome") ? !event.target.value.length ? true : false : values.nomeInputError, 
            valorInputError: (prop === "valorInputError" || prop === "valor") ? !event.target.value.length ? true : false : values.valorInputError,
            grupoInputError: (prop === "grupoInputError" || prop === "grupo") ? !event.target.value.length ? true : false : values.grupoInputError,
            descricaoInputError: (prop === "descricaoInputError" || prop === "descricao") ? !event.target.value.length ? true : false : values.descricaoInputError       
        });
    };

    const handleInputError = (prop: any) => (event: React.FocusEvent<HTMLInputElement>) => {
        setValues({
            ...values,
            [prop]: !event.target.value.length ? true : false            
        });        
    }

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 600,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const handleSelProd = (prod: IProducts|any, idx: number|any) => {        
        setProdutoInfo(prod);
        handleOpenModalDetail();
    }

    const handleCadProd = async () => {
        if ((values.idInputError === true || values.nomeInputError === true || 
            values.valorInputError === true || values.grupoInputError === true || 
            values.descricaoInputError === true)) return;

        let newProdData = {
            id: values.id,
            nome: values.nome,
            valor: parseFloat(values.valor),
            grupo: values.grupo.toLowerCase(),
            vendido: true,
            dataVenda: getCadDate(),
            descricao: values.descricao
          }
        
        let resp = await registerProduct(newProdData);

        if (resp.status >= 200 && resp.status <= 299) {
            dispatch(getProducts());
        }

        handleCleanCadData();
        handleCloseModalCad();
    }

    const handleDelProd = async (prod: IProducts|any) => {        
        setProdutoInfo(prod);

        let resp = await deleteProduct(prod.id);

        if (resp.status >= 200 && resp.status <= 299) {
            dispatch(getProducts());
        }

        handleCloseModalDel();
    }

    function generate() {
        return products.map((prod: any, idx: any) => 
            <ListItem
                className={idx % 2 === 0 ? 'products-list-item-box-color-even' : 'products-list-item-box-color-odd'}
                key={idx}
                secondaryAction={
                    <div className="produtos-edit-icons">
                        <div placeholder="Detalhar Produto" className="produto-edit-icon">
                            <Tooltip title="Detalhar Produto">
                                <IconButton 
                                    edge="end" 
                                    aria-label="detail" 
                                    onClick={() => {handleSelProd(prod, idx)}}                                                   
                                >
                                    <AssignmentIcon />                                  
                                </IconButton>
                            </Tooltip>
                        </div>
                        <div className="produto-edit-icon">
                            <Tooltip title="Deletar Produto">
                                <IconButton 
                                    edge="end" 
                                    aria-label="delete"
                                    onClick={() => {handleOpenModalDel(prod, idx)}}
                                >
                                    <DeleteIcon />                                
                                </IconButton>
                            </Tooltip>
                        </div>
                    </div>
                }
            >
                <div className="produtos-list-box-infos">
                    <div className="products-list-item-info">
                        <div className="products-list-item-info-title">ID:</div>
                        <div className="products-list-item-info-text">
                            <ListItemText 
                                primary={prod.id}
                            />
                        </div>
                    </div>
                    <div className="products-list-item-info">
                        <div className="products-list-item-info-title">NOME:</div>
                        <div className="products-list-item-info-text">
                            <ListItemText 
                                primary={prod.nome}
                            />
                        </div>
                    </div>
                    <div className="products-list-item-info">
                        <div className="products-list-item-info-title">VALOR:</div>
                        <div className="products-list-item-info-text">
                            <ListItemText 
                                primary={prod.valor}
                            />
                        </div>
                    </div>
                    <div className="products-list-item-info">
                        <div className="products-list-item-info-title">GRUPO:</div>
                        <div className="products-list-item-info-text">
                            <ListItemText 
                                primary={prod.grupo}
                            />
                        </div>
                    </div>
                </div>
            </ListItem>
        );
    }

    return (
        <div>
            <Modal
                open={modalDetail}
                onClose={() => {handleCloseModalDetail()}}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography className="produto-modal-detalhe-titulo" id="modal-modal-detalhe-title" variant="h6" component="h2">
                        Detalhamento do Produto
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <span>
                            <span className="products-list-item-info">
                                <span className="products-list-item-info-title">ID:</span>
                                <span className="products-list-item-info-text">
                                    {produtoInfo.id}
                                </span>
                            </span>
                            <span className="products-list-item-info">
                                <span className="products-list-item-info-title">NOME:</span>
                                <span className="products-list-item-info-text">
                                    {produtoInfo.nome}
                                </span>
                            </span>
                            <span className="products-list-item-info">
                                <span className="products-list-item-info-title">VALOR:</span>
                                <span className="products-list-item-info-text">
                                    {produtoInfo.valor}
                                </span>
                            </span>
                            <span className="products-list-item-info">
                                <span className="products-list-item-info-title">GRUPO:</span>
                                <span className="products-list-item-info-text">
                                    {produtoInfo.grupo}
                                </span>
                            </span>
                            <span className="products-list-item-info">
                                <span className="products-list-item-info-title">DESCRIÇÃO:</span>
                                <span className="products-list-item-info-descricao">
                                    {produtoInfo.descricao}
                                </span>
                            </span>
                        </span>
                    </Typography>
                </Box>
            </Modal>
            <Modal
                open={modalDel}
                onClose={() => {handleCloseModalDel()}}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography className="produto-modal-delete-titulo" id="modal-modal-delete-title" variant="h6" component="h2">
                        {
                            <span>
                                <span>
                                    {"Deseja realmente excluir o produto?"}
                                </span>
                                <p>
                                    {produtoInfo.nome}
                                </p>
                            </span>
                        }
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <span className="modal-buttons-container">
                            <Button
                                variant="contained"
                                className="std-button modal-button-yes"   
                                onClick={() => {handleDelProd(produtoInfo)}}                                                
                            >
                                {'Sim'}
                            </Button>
                            <Button
                                variant="contained"
                                className="modal-button-no"   
                                onClick={() => {handleCloseModalDel()}}                 
                            >
                                {'Não'}
                            </Button>
                        </span>
                    </Typography>
                </Box>
            </Modal>
            <Dialog
                open={modalCad}
                onClose={() => {handleCloseModalCad()}}                
                PaperProps={{ sx: { minWidth: "600px !important", height: "700px", scroll: "none" } }}                
            >
                <DialogTitle className="produto-modal-cadastro-titulo">
                    Cadastrar Novo Produto
                </DialogTitle>
                <DialogContent>
                    <span className="products-list-item-info">
                        <span className="products-list-item-info-modal-title">ID:</span>
                        <span className="products-list-item-info-modal-text">
                            <TextField
                                error={values.idInputError}
                                id="modal-id-field"
                                label={'ID'}
                                type="text"
                                className="modal-input"
                                value={values.id}
                                onChange={handleChange("id")}
                                onBlur={handleInputError("idInputError")}
                                helperText={values.idInputError ? "Campo necessário" : ""}
                            />
                        </span>
                    </span>
                    <span className="products-list-item-info">
                        <span className="products-list-item-info-modal-title">NOME:</span>
                        <span className="products-list-item-info-modal-text">
                            <TextField
                                error={values.nomeInputError}
                                id="modal-nome-field"
                                label={'Nome'}
                                type="text"
                                className="modal-input"
                                value={values.nome}
                                onChange={handleChange("nome")}
                                onBlur={handleInputError("nomeInputError")}
                                helperText={values.nomeInputError ? "Campo necessário" : ""}
                            />
                        </span>
                    </span>
                    <span className="products-list-item-info">
                        <span className="products-list-item-info-modal-title">VALOR:</span>
                        <span className="products-list-item-info-modal-text">
                            <TextField
                                error={values.valorInputError}
                                id="modal-valor-field"
                                label={'Valor'}
                                type="number"
                                className="modal-input"
                                value={values.valor}
                                onChange={handleChange("valor")}
                                onBlur={handleInputError("valorInputError")}
                                helperText={values.valorInputError ? "Campo necessário" : ""}
                            />
                        </span>
                    </span>
                    <span className="products-list-item-info">
                        <span className="products-list-item-info-modal-title">GRUPO:</span>
                        <span className="products-list-item-info-modal-text">
                            <TextField
                                error={values.grupoInputError}
                                id="modal-grupo-field"
                                label={'Grupo'}
                                type="text"
                                className="modal-input"
                                value={values.grupo}
                                onChange={handleChange("grupo")}
                                onBlur={handleInputError("grupoInputError")}
                                helperText={values.grupoInputError ? "Campo necessário" : ""}
                            />
                        </span>
                    </span>
                    <span className="products-list-item-info">
                        <span className="products-list-item-info-modal-title">DESCRIÇÃO:</span>
                        <span className="products-list-item-info-modal-descricao">
                            <TextField
                                error={values.descricaoInputError}
                                id="modal-descricao-field"
                                label={'Descrição'}
                                type="text"
                                className="modal-input"
                                value={values.descricao}
                                onChange={handleChange("descricao")}
                                onBlur={handleInputError("descricaoInputError")}
                                helperText={values.descricaoInputError ? "Campo necessário" : ""}
                            />
                        </span>
                    </span>
                </DialogContent>
                <DialogActions>
                    <Box className="btn-cad-dialog">
                        <Button
                            variant="contained"
                            className="std-button modal-button-yes"   
                            onClick={() => {handleCadProd()}}                                                
                        >
                            {'Cadastrar'}
                        </Button>
                        <Button
                            variant="contained"
                            className="modal-button-no"   
                            onClick={() => {handleCloseModalCad()}}                 
                        >
                            {'Sair'}
                        </Button>
                    </Box>
                </DialogActions>                
            </Dialog>
            <Grid>
                <Box>
                    <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
                        Listagem de Produto(s)
                    </Typography>
                    <Button
                        variant="contained"
                        className="std-button user-input"   
                        onClick={() => {handleOpenModalCad()}}                 
                    >
                        {'Cadastrar Produto'}
                    </Button>
                </Box>
                <Box className="produtos-list-main-container">
                    <List dense={true}>
                        {
                            products && products.length ?
                                generate()
                            :
                                <></>
                        }
                    </List>
                </Box>
            </Grid>
        </div>
    )
}

export default Produtos;