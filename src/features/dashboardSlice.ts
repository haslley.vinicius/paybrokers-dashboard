import { 
    createSlice, 
    PayloadAction,
    createAsyncThunk 
} from "@reduxjs/toolkit";
import { RootState } from '../store/store';
import axios from "axios";
import { IProducts } from '../interfaces/dashboard.interface';

export interface IDashboardState {
    products: IProducts[]|any[];
    status?: string;
    error?: any;
}

const initialState: IDashboardState = {
    products: [],
    status: 'idle', //'idle' | 'pending' | 'succeeded' | 'failed'
    error: null
}

export const getProducts = createAsyncThunk('/getProducts', async () => {
    try {
        const response = await axios.get('http://localhost:3500/products');
        return [...response.data];
    } catch (err: any) {
        return err.message;
    }
})

const dashboardSlice = createSlice({
    name: 'dashboard',
    initialState,
    reducers: {
        setProductsData: (state, action: PayloadAction<any>) => {
            state = {
                ...state,
                products: action.payload.products
            } 
        }
    },
    extraReducers(builder) {
        builder
            .addCase(getProducts.pending, (state, action) => {
                state = {
                    ...state,
                    status: 'pending'
                }

                return state;
            })
            .addCase(getProducts.fulfilled, (state, action: PayloadAction<any>) => {
                state = {
                    ...state,
                    status: 'succeeded',
                    products: action.payload
                }

                localStorage.setItem('products', JSON.stringify(action.payload))

                return state;
            })
            .addCase(getProducts.rejected, (state, action) => {
                state = {
                    ...state,
                    status: 'failed',
                    error: action.error.message
                }
                
                return state;
            })
    }
});

export const getProductsStatus = (state: RootState) => state.dashboard.status;
export const getProductsError = (state: RootState) => state.dashboard.error;

export const { setProductsData } = dashboardSlice.actions;

export default dashboardSlice.reducer;