import React, { useState, useEffect } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { IProducts } from '../../interfaces/dashboard.interface';
import { Box, TextField } from '@mui/material';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const BarChart = ({products}: IProducts | any) => {
  const [fatorVenda, setFatorVenda] = useState(3);

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
        text: 'Total de vendas por dia/mês corrente',
      },
    },
  };

  function getDaysInMonth(year: any, month: any) {
    return new Date(year, month, 0).getDate();
  }

  const getMonthDays = () => {
    const date = new Date();
    const currYear = date.getFullYear();
    const currMonth = date.getMonth() + 1;
    const monthDays = getDaysInMonth(currYear, currMonth);

    let daysArr = [];
    for (let c = 1; c <= monthDays; c++) { daysArr[c] = c.toString().length === 1 ? '0'+c : c; }

    return daysArr;
  }

  const labels = getMonthDays();

  const [data, setData] = useState({
    labels,
    datasets: [
      {
        label: 'Est. Vendas/Dia',
        data: [0],
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Vendas/Dia',
        data: [0],
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ]});

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {    
    if (event.target.value.length) { 
      let checkFactor = parseInt(event.target.value) < 3 ? 3 : parseInt(event.target.value);
      setFatorVenda(checkFactor);
    }
  };

  const getTotalVendasDias = () => {
    let totalVendasDias: any[] = [];
    let somaVendasDia: number = 0;

    for (let c1 = 0; c1 < labels.length; c1++) {
      for (let c2 = 0; c2 < products.length; c2++) {
        if (c1 > 0) {
          let prodDataVenda = products[c2].dataVenda.substring(0, 2); 
          if (prodDataVenda === labels[c1].toString()) {
            if (products[c2].vendido) {
              somaVendasDia += products[c2].valor;
            }
          }
        }
      }
      totalVendasDias.push(somaVendasDia);
      somaVendasDia = 0;
    }
    
    return totalVendasDias.length ? totalVendasDias : [0];  
  }

  useEffect(() => {
    if (products) {
      setData({
        labels,
        datasets: [
          {
            label: 'Est. Vendas/Dia',
            data: getTotalVendasDias().map(e => fatorVenda * e),
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          },
          {
            label: 'Vendas/Dia',
            data: getTotalVendasDias(),
            backgroundColor: 'rgba(53, 162, 235, 0.5)',
          },
        ],
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[products, fatorVenda])

  return (
    <Box>
      <TextField
        id="sell-factor"
        label={'(Est. (Fator Multip.) x val. prod.) min: 3x'}
        type="number"
        className="user-input"
        value={fatorVenda}
        onChange={handleChange}
      />
      <Bar options={options} data={data} />
    </Box>
  )
} 