import { 
    createSlice, 
    PayloadAction 
} from "@reduxjs/toolkit";
import { ILoginState } from "../interfaces/login.interface";

const initialState: ILoginState = {
    userName: "",
    password: ""
}

const loginSlice = createSlice({
    name: 'login',
    initialState,
    reducers: {
        userLogin: (state, action: PayloadAction<any>) => {  
            state = {
                ...state, 
                userName: action.payload.user, 
                password: action.payload.password
            }

            return state;
        }
    }
});

export const { userLogin } = loginSlice.actions;

export default loginSlice.reducer;