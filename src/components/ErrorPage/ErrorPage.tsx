import {
    Box
} from "@mui/material";
import PayBrokersLogo from '../../assets/images/paybrokers_logo.png';

const ErrorPage = () => {    
    return (
        <Box className="login-container">
            <Box>
                <Box style={{ marginBottom: "30px" }}>
                    <img src={PayBrokersLogo} alt="paybrokers_logo" />
                    <div>{'PAGE NOT FOUND'}</div>
                </Box>                            
            </Box>      
        </Box>
    )
}

export default ErrorPage;