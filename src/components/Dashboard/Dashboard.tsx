import React, { useEffect, useState } from 'react';
import {
    Box,
    Button
} from '@mui/material';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import {
    useAppSelector,
    useAppDispatch
} from '../../store/hooks';
import { useNavigate } from "react-router-dom";
import PayBrokersLogo from '../../assets/images/paybrokers_logo.png';
import type { RootState } from '../../store/store';
import { userLogin } from '../../features/loginSlice';
import { PieChart } from '../Charts/PieChart';
import { BarChart } from '../Charts/BarChart'; 
import Produtos from '../Produtos/Produtos';
import { getProducts } from '../../features/dashboardSlice';

const Dashboard = () => {
    const [showProdutos, setShowProdutos] = useState(false);
    const [userName, products, status] = useAppSelector((state: RootState) => [
        state.login.userName, 
        state.dashboard.products,
        state.dashboard.status
    ]);
   
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const dispatch = useAppDispatch();   
    const navigate = useNavigate();
    
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleShowProdutos = () => {
        setShowProdutos(true);
        setAnchorEl(null);
    }

    const handleLogout = () => {
        const userData = {
            user: "",
            password: ""
        };

        localStorage.removeItem('userName');
        localStorage.removeItem('products');
        localStorage.removeItem('isLogged');

        setAnchorEl(null);

        dispatch(userLogin(userData));

        navigate("/");
    };

    const acquireProducts = () => {
        if (products.length) return products;

        let storageProducts = localStorage.getItem('products');
        if (storageProducts) return JSON.parse(storageProducts);
    }

    useEffect(() => {   
        if (!Boolean(localStorage.getItem('isLogged'))) handleLogout();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (status === 'idle') dispatch(getProducts());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [status])
    
    return (
        <Box>
            <Box className="dahsboard-header primary-background-color-2">
                <div>
                    <Button
                        id="basic-button"
                        aria-controls={open ? 'basic-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                        className="std-button"
                    >
                        MENU
                    </Button>
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'basic-button',
                        }}
                    >                 
                        <MenuItem onClick={handleShowProdutos}>Produtos</MenuItem>                    
                        <MenuItem onClick={handleLogout}>Logout</MenuItem>
                    </Menu>
                </div>
                <div className="info-logged-user">
                    <div>
                        {'Logado: '}
                    </div>
                    <div className="user-info">{userName ? userName : localStorage.getItem('userName')}</div>
                </div>
            </Box>
            <Box className="dashboard-cotent">
                {
                    showProdutos ?
                        <div className="produtos-container">
                            <Produtos products={acquireProducts()} />
                        </div>
                    :
                        <div className="charts-container">
                            <div className="bar-chart">
                                <BarChart products={acquireProducts()} />
                            </div>
                            <div className="pie-chart">
                                <PieChart products={acquireProducts()} />
                            </div>
                        </div>
                }
            </Box>
            <Box className="dashboard-footer primary-background-color-1">
                <div className="logo-container">
                    <div className="footer-logo">
                        <img className="logo-image-footer" src={PayBrokersLogo} alt="paybrokers_logo" />
                    </div>
                </div>
            </Box>
        </Box>
    )
}

export default Dashboard;