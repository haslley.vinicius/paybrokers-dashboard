import axios from 'axios';

const execLogin = async (user: string, password: string) => {    
    const resp = await axios.get(`http://localhost:3500/users/?user=${user}&password=${password}`)
            
    return [...resp.data];
}

export {
    execLogin
}