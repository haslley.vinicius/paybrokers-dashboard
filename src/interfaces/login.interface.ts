export interface ILoginState {
    userName: string;
    password: string;
}