import React from 'react';
import '../styles/App.css';
import { 
  BrowserRouter, 
  Routes, 
  Route 
} from "react-router-dom";
import ViewLogin from './Login';
import ViewDashboard from './Dashboard';
import ViewErrorPage from './ErrorPage';

function App() {
  return (    
    <div className="App">                  
      <BrowserRouter>        
        <Routes>
          <Route path="/" element={<ViewLogin />} />
          <Route path="/dashboard" element={<ViewDashboard />} />          
          <Route path="*" element={<ViewErrorPage />} />
        </Routes>        
      </BrowserRouter>            
    </div>
  );
}

export default App;
