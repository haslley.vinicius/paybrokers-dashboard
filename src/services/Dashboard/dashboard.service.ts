import axios from 'axios';

const getProducts = async () => {    
    const resp: any = await axios.get(`http://localhost:3500/products`);
            
    return [...resp.data];
}

export {
    getProducts
}