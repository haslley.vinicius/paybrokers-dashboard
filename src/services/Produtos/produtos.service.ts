import axios from 'axios';
import { IProducts } from '../../interfaces/dashboard.interface';

const registerProduct = async (product: IProducts|any) => {    
    const resp = await axios.post('http://localhost:3500/products/', product);
    return resp;
}

const deleteProduct = async (product: IProducts|any) => {    
    const resp = await axios.delete(`http://localhost:3500/products/${product}`);
    return resp;
}

export {
    registerProduct,
    deleteProduct
}